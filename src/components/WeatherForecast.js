import React from 'react';
import moment from 'moment';

const WeatherForecast = ({weatherForecasts}) => {
  const elements = [];

  weatherForecasts.forEach((weatherForecast, key) => {
    elements.push(
      <div key={key} className="col-3">
        <div className="card shadow-sm rounded">
          <div className="card-body">
            <h5 className="card-title">{weatherForecast.description}</h5>
            <p className="card-text">{moment(weatherForecast.date).format('dddd, Do MMM')}</p>
            <p className="card-text">High: {weatherForecast.temperatureRange.high}&#8451;</p>
            <p className="card-text">Low: {weatherForecast.temperatureRange.low}&#8451;</p>
          </div>
        </div>
      </div>
    );
  });

  return elements;
}

export default WeatherForecast;
