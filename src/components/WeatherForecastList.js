import React from 'react';
import WeatherForecast from './WeatherForecast';

const WeatherForecastList = ({weatherForecasts}) => {
  const elements = [];

  weatherForecasts.forEach((weatherForecast, key) => {
    elements.push(<div key={key} className="row justify-content-between mb-5">
      <div className="col-12 mb-5">
        <h1>{weatherForecast.source}</h1>
      </div>
      <WeatherForecast weatherForecasts={weatherForecast.weather}></WeatherForecast>
    </div>);
  });

  return elements;
}

export default WeatherForecastList;
