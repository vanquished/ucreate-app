import React from 'react';
import { withRouter } from 'react-router-dom';
import UserService from '../../services/UserService';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  handleUsernameChange = (e) => {
    this.setState({
      username: e.target.value
    });
  }

  handlePasswordChange = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const username = this.state.username;
    const password = this.state.password;

    if (this.validateUsername(username) && this.validatePassword(password)) {
      const userService = new UserService();
      userService.loginUser(username, password)
      .then(user => {
        const userId = user.data.id;
        const apiToken = user.data.apiToken;
        this.props.history.push({pathname: '/', state: {userId: userId, apiToken: apiToken}});
      })
      .catch(e => {});
    }
  }

  handleSignUp = (e) => {
    const username = this.state.username;
    const password = this.state.password;

    if (this.validateUsername(username) && this.validatePassword(password)) {
      const userService = new UserService();
      userService.createUser(username, password)
      .then(user => {
        const userId = user.data.userId;
        const apiToken = user.data.apiToken;
        this.props.history.push({pathname: '/', state: {userId: userId, apiToken: apiToken}});
      })
      .catch(e => {});
    }
  }

  validateUsername(username) {
    if (username.length > 0 && username.length <= 25) {
      return true;
    } else {
      return false;
    }
  }

  validatePassword(password) {
    if (password.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return <div className="row justify-content-center text-center">
      <div className="col-6">
        <form onSubmit={this.handleSubmit}>
          <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
          <label htmlFor="inputUsername" className="sr-only">Username</label>
          <input
            type="text"
            id="inputUsername"
            className="form-control"
            placeholder="Username"
            value={this.state.username}
            onChange={this.handleUsernameChange} />
          <label htmlFor="inputPassword" className="sr-only">Password</label>
          <input
            type="password"
            id="inputPassword"
            className="form-control"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handlePasswordChange} />
          <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
          <button className="btn btn-lg btn-primary btn-block" type="button" onClick={this.handleSignUp}>Sign up</button>
        </form>
      </div>
    </div>;
  }
}

export default withRouter(Login);