import React from 'react';
import { withRouter } from 'react-router-dom';
import WeatherForecastList from '../WeatherForecastList';
import Loading from '../Loading';
import WeatherService from '../../services/WeatherService';
import CityService from '../../services/CityService';
import CityForm from '../CityForm';

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: null,
      isLoaded: false,
      weatherForecasts: [],
      selectedCity: null,
      cities: [],
    };
  }

  componentDidMount() {
    if (
      this.props.location.state === undefined
      || !'userId' in this.props.location.state
      || !'apiToken' in this.props.location.state
    ) {
      this.props.history.push('/login');
    } else {
      const userId = this.props.location.state.userId;
      const apiToken = this.props.location.state.apiToken;
      const cityService = new CityService();

      this.setState({userId: userId});

      cityService.getCities(apiToken, userId)
      .then(
        (result) => {
          const { data } = result;
          const cities = [];

          data.forEach(city => {
            cities.push(city);
          });

          this.setState({cities: cities});
        },
        (error) => {}
      );

      cityService.getCitiesByUserId(apiToken, userId)
      .then(
        (result) => {
          const { data } = result;
          const days = 3;
          const sources = [
            'openweathermap',
            'weatherapi',
            'weatherbit'
          ];
          const cities = data.map(e => e.name).join(',');
          const countryCodes = data.map(e => e.countryCode).join(',');

          this.setState({selectedCity: data[0]});

          const weatherService = new WeatherService();
          const weatherPromises = [];

          sources.forEach(source => {
            weatherPromises.push(weatherService.getForecast(apiToken, cities, countryCodes, days, source)
            .then(
              (weather) => {
                this.setState(state => {
                  const weatherForecasts = state.weatherForecasts.concat([weather.data])
                  return {weatherForecasts};
                })
              },
              (error) => {}
            ));
          });

          Promise.all(weatherPromises)
          .then(() => {
            this.setState({isLoaded: true});
          })
        },
        (error) => {}
      );
    }
  }

  render() {
    return (this.state.isLoaded)
      ? <div>
          <CityForm userId={this.state.userId} city={this.state.selectedCity} cities={this.state.cities}></CityForm>
          <WeatherForecastList weatherForecasts={this.state.weatherForecasts}></WeatherForecastList>
        </div>
      : <div className="text-center"><Loading></Loading></div>;
  }
}

export default withRouter(Home);