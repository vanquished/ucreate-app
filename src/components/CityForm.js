import React from 'react';
import UserService from '../services/UserService';

class CityForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedCity: ''
    };
  }

  componentDidMount() {
    // TODO: Can't get this to work
    const selectedCity = this.props.cities.find(city => city.id === this.props.city.id);
    this.setState({selectedCity: selectedCity});
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const userService = new UserService();
    userService.updateUserCity(this.props.userId, this.state.selectedCity)
    .then(
      (result) => {},
      (error) => {}
    );
  }

  handleChange = (e) => {
    this.setState({selectedCity: e.target.value});
    console.log(this.state.selectedCity);
  }

  render() {
    const elements = [];

    this.props.cities.forEach((city, key) => {
      elements.push(<option key={key} value={city.id}>{city.name}</option>);
    });

    return <form onSubmit={this.handleSubmit}>
      <select value={this.state.selectedCity} onChange={this.handleChange}>
        {elements}
      </select>
      <button>Set</button>
    </form>;
  }
}

export default CityForm;
