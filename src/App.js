import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.scss';
import Login from './components/page/Login';
import Home from './components/page/Home';

function App() {
  return (
    <Router>
      <header className="bg-dark mb-5 text-white">
        <div className="container justify-content-between p-3">
          <div>Weather App</div>
        </div>
      </header>
      <main className="container">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
        </Switch>
      </main>
      <footer></footer>
    </Router>
  );
}

export default App;
