class UserService {
  loginUser(username, password) {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8080/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: username,
          password: password
        })
      })
      .then(res => {
        if (res.status === 200) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    });
  }

  createUser(username, password) {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8080/users', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: username,
          password: password
        })
      })
      .then(res => {
        if (res.status === 201) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    });
  }

  updateUserCity(userId, cityId) {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8080/users/city', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          userId: userId,
          cityId: cityId
        })
      })
      .then(res => {
        if (res.status === 200) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    });
  }
}

export default UserService;
