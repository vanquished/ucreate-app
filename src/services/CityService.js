class CityService {
  getCities(apiToken, userId) {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:8080/cities`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${apiToken}`,
          'Content-Type': 'application/json'
        }
      })
      .then(res => {
        if (res.status === 200) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    })
  }

  getCitiesByUserId(apiToken, userId) {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:8080/cities`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${apiToken}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({userId: userId})
      })
      .then(res => {
        if (res.status === 200) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    })
  }
}

export default CityService;
