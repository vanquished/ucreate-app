class WeatherService {
  getForecast(apiToken, cities, countryCodes, days, source) {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:8080/weather?cities=${cities}&countryCodes=${countryCodes}&days=${days}&source=${source}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${apiToken}`
        },
      })
      .then(res => {
        if (res.status === 200) {
          return res.json().then((result) => resolve(result));
        } else {
          return reject('Server error');
        }
      })
      .catch(error => reject('Server error'));
    });
  }
}

export default WeatherService;
